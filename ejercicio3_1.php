<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <style>
            table,td{
                border: 1px solid black;
                border-collapse: collapse;
                padding: 7px 15px;
                background-color: grey;
                color:white;
                font-family: arial;
            }
        </style>
    </head>
    <body>
        <?php
        $nombre=$_GET["nombre"];
        $edad=$_GET["edad"];
        $poblacion=$_GET["poblacion"];
        ?>
        <table>
            <tr>
                <td>Nombre</td>
                <td><?= $nombre ?></td>
            </tr>
            <tr>
                <td>Edad</td>
                <td><?= $edad ?></td>
            </tr>
            <tr>
                <td>Población</td>
                <td><?= $poblacion ?></td>
            </tr>
        </table>
        <?php
        
        ?>
    </body>
</html>
