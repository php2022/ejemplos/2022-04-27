<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        $circulos = [
            20,30,40
        ];
        
        $cuadrados = [
            20,30  
        ];
        
        ?>
        
        <?php
        //imprimo los circulos
        foreach ($circulos as $value) {
            //comienzo de impresion de circulos 
        ?>
                
        <?xml version="1.0" encoding="UTF-8" standalone="no"?>
        <svg width="140" height="140" viewBox="-10 -10 140 140" style="background-color: white">
        <circle cx="60" cy="60" r="<?= $value ?>" fill="black" />
        </svg>
        
        <?php
        //final de impresion de circulos
        }
        ?>
        
        <?php
        //imprimo los cuadrados
        foreach ($cuadrados as $value) {
            //comienzo de impresion de cuadrados 
        ?>
        
        <?xml version="1.0" encoding="UTF-8" standalone="no"?>
        <svg width="140" height="140" viewBox="-10 -10 140 140" style="background-color: white">
        <rect x="10" y="10" width="<?= $value ?>" height="<?= $value ?>"/>
        </svg>
        
        <?php
        //final de impresion de cuadrados
        }
        ?>
        
        
        
    </body>
</html>
