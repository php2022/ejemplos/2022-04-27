<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <form>
            <label for="num1">Numero1</label>
            <input type="number" name="numeros[]" id="num1"/>
            <label form="num2">Numero 2</label>
            <input type="number" name="numeros[]" id="num2"/>            
            <button>Enviar</button>            
        </form>
        
        <?php
            if($_GET){
                // ahora leo los dos numeros como array
                $numeros = $_GET["numeros"];
                
                // creo un array asociativo para almacenar los resultados
                $resultados=[
                    "suma" => $numeros[0]+$numeros[1],
                    "resta" => $numeros[0]-$numeros[1],
                    "producto" => $numeros[0]*$numeros[1],
                    "cociente" => $numeros[0]/$numeros[1],
                    "potencia" => $numeros[0]**$numeros[1],
                    "modulo" => $numeros[0]%$numeros[1],
                ]
                
        ?>
        <table border="1" style="text-align: center; margin: 5px" cellspacing="3">
                    <tr>
                        <td>Suma</td>
                        <td><?= $resultados["suma"] ?></td>
                    </tr>
                    <tr>
                        <td>Resta</td>
                        <td><?= $resultados["resta"] ?></td>
                    </tr>
                    <tr>
                        <td>Multiplicacion</td>
                        <td><?= $resultados["producto"] ?></td>
                    </tr>
                    <tr>
                        <td>Division</td>
                        <td><?= $resultados["cociente"] ?></td>
                    </tr>
                    <tr>
                        <td>Potencia</td>
                        <td><?= $resultados["potencia"] ?></td>
                    </tr>
                    <tr>
                        <td>Modulo</td>
                        <td><?= $resultados["modulo"] ?></td>
                    </tr>
            </table>
        <?php
            }
        ?>
    </body>
</html>
