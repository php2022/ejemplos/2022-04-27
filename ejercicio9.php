<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php
        // array enumerado
        $menu = [
            "Home",
            "Quienes somos",
            "Productos",
        ];
        // OPCION 1
        /*
        echo "<ul>";
        foreach ($menu as $value){
            echo "<li>$value</li>";            
        }
        echo "</ul>"
        */
        ?>
        <!-- OPCION 2 -->
        <ul>
            <?php
            //inicio bucle
                foreach ($menu as $value){
            ?>
                <li><?= $value ?></li>            
             <?php
             // fin bucle
                }
            ?>
        </ul>
        
     
        <?php
        //array asociativo
        $menu1 = [
            "Home" => "index.php",
            "Quienes somos" => "quienes.php",
            "Productos" => "productos.php"
        ];
        // OPCION 1
        /*
        echo "<ul>";
        foreach ($menu1 as $indice=>$valor){
            echo "<li><a href=\"$valor\">$indice</a></li>";           
        }
        echo "</ul>";    
         */  
        ?>
        
        <!-- OPCION 2 -->
        <ul>
            <?php
            //inicio bucle
                foreach ($menu1 as $indice=>$valor){
            ?>
                <li><a href="<?= $valor?>"><?= $indice ?></a></li>            
             <?php
             // fin bucle
                }
            ?>
        </ul>
         
        <?php
        // array dos dimensiones
        // primera array enumerado
        // segunda array asociativo
        $datos=[
            [
                "id" => 1,
                "nombre" => "Jose",
            ],
            [
                "id" => 2,
                "nombre" => "Ana",
            ]
        ];        
        
        ?>   
        
            <?php
            /* OPCION 1 */
            /*echo "<table>";
            echo "<tr>";
            foreach ($datos[0] as $key=>$value){
                echo "<td> $key </td>";
            }    
            echo "</tr>";
            echo "<tr>";
                foreach ($datos as $dato){
                    echo"<tr>";
                        foreach ($dato as $value) {
                            echo "<td> $value </td>";
                        }
                    echo"</tr>";
                }  

            echo "</tr>";
            echo "</table>";*/
            ?>
    <!-- OPCION 2 -->
    <table>                
            <tr>
            <?php
                foreach ($datos[0] as $key=>$value){
                    echo "<td> $key </td>";
                }
            ?>    
            </tr>            
            <tr>
            <?php
                foreach ($datos[0] as $dato){
                    echo "<td> $dato </td>";
                }
            ?>   
            </tr>
            <tr>
                <?php
                foreach ($datos[1] as $dato){
                    echo "<td> $dato </td>";
                }
            ?>   
            </tr>
        </table>
   
    </body>
</html>

